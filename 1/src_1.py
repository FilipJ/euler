#!/usr/bin/env python3
limit = 1000 # later 1000

# naive approach:
s = 0
for i in range(limit):
    if i%3 == 0 or i%5 == 0:
        s = s + i
print(f"result is {s}")

# List comprehention approach:
s = sum([x for x in range(limit) if (x%3 == 0 or x%5 == 0)])
print(f"result is: {s}")