#!/usr/bin/env python3

# naive approach using generators
def fib():
    prev2 = 1
    yield prev2
    prev1 = 2
    yield prev1
    while True:
        cur = prev1 + prev2
        yield cur
        prev2, prev1 = prev1, cur

fib_gen = fib()
s = 0
while True:
    n = next(fib_gen)
    if n > 4000000:
        break
    if n%2 == 0:
        s += n
print(f"result is: {s}")

# Approach with list and filter
fib_list = [1, 2]
while fib_list[-1] < 4000000:
    fib_list.append(fib_list[-1] + fib_list[-2])
print(f"result is: {sum(filter(lambda x: x % 2 == 0, fib_list))}")
