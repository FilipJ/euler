#!/usr/bin/env python3

def is_prime(n):
    if n <= 3:
        return True
    if n % 2 == 0 or n % 3 == 0:
        return False
    for i in range (4, n-1):
        if n % i == 0:
            return False
    return True

def find_factor(n):
    for i in range(2, n):
        if n % i == 0 and is_prime(i):
            return i

num = 600851475143
factors = []
while True:
    f = find_factor(num)
    factors.append(f)
    print(f"found factor: {f}")
    num = num // f
    if is_prime(num):
        factors.append(num)
        print(f"found factor: {num}")
        break
print(f"result is: {factors[-1]}")

# Chat GPTs solution:
n = 600851475143
i = 2
largest_factor = 1

while i * i <= n:
    if n % i:
        i += 1
    else:
        n //= i
        largest_factor = i

if n > largest_factor:
    largest_factor = n

print(largest_factor)