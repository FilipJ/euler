# Project Euler

My personal (login *Fijassko*) implementation of problems from [Project Euler](https://projecteuler.net/).

I'll be probably using multiple programming languages - first plan is Python and C++.

Folders are named by numbers - corresponding with ID of problem its containing.